<?php

// Plugin definition
$plugin = array(
  'title' => t('ERPAL login'),
  'category' => t('ERPAL'),
  'icon' => 'erpallogin.png',
  'theme' => 'erpallogin',
  'css' => 'erpallogin.css',
  'regions' => array(
    'body' => t('Body')
  ),
);
